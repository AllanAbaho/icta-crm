<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use app\components\ActiveRecordLogger;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "events".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $cost
 * @property int|null $cpd_points
 * @property string|null $start_time
 * @property string|null $end_time
 * @property string|null $start_date
 * @property string|null $end_date
 * @property string|null $description
 * @property string|null $updated_at
 * @property string|null $created_at
 * @property int|null $updated_by
 * @property int|null $created_by
 * @property string|null $venue
 */
class Events extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'events';
    }

    public function behaviors()
    {
        return [
            ['class'=>ActiveRecordLogger::className()],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cpd_points', 'updated_by', 'created_by'], 'integer'],
            [['start_date', 'end_date', 'updated_at', 'created_at'], 'safe'],
            [['description'], 'string'],
            [['name', 'cost', 'start_time', 'end_time', 'venue','file'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'cost' => 'Cost',
            'cpd_points' => 'Cpd Points',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'description' => 'Description',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'created_by' => 'Created By',
            'venue' => 'Venue',
        ];
    }
}
