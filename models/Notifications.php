<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notifications".
 *
 * @property int $id
 * @property int $user_id
 * @property string $status
 * @property string $message
 * @property string $type
 * @property string $created_at
 */
class Notifications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'message', 'type', 'created_at'], 'required'],
            [['user_id'], 'integer'],
            [['message'], 'string'],
            [['created_at'], 'safe'],
            [['status', 'type'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'status' => 'Status',
            'message' => 'Message',
            'type' => 'Type',
            'created_at' => 'Created At',
        ];
    }
}
