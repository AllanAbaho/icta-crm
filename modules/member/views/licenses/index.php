<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Licenses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="licenses-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <p>
        <?= Html::a('Create Licenses', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'user.name',
            'start_date',
            'end_date',
            [
                'attribute' => 'file',
                'value' => function($data){
                    if($data->file){
                        $file = \Yii::getAlias('@app')."/web/licenses/" . $data->file;
                        if(file_exists($file)) {
                            return Html::a('Download License',['/download/index','file'=>$data->file]);
                        }else{
                            return "No Attachment";
                        }    
                    }
                },
                'format'=>'html',
            ],
            //'updated_at',
            //'created_at',
            //'updated_by',
            //'created_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>



</div>
