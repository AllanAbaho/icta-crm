<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'payment_type')->textInput(['value' => 'Bank', 'readonly'=>true]) ?>

    <?= $form->field($model, 'amount')->textInput(['value'=>$application->user->membership->fee]) ?>

    <?= $form->field($model, 'status')->hiddenInput(['value' => 'Paid'])->label(false) ?>

    <?= $form->field($model, 'receipt')->fileInput(['required'=>true]); ?>

    <?= $form->field($model, 'name')->hiddenInput(['value'=>$application->user->name])->label(false) ?>

    <?= $form->field($model, 'phone_number')->hiddenInput(['value'=>$application->user->primary_phone_number])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
