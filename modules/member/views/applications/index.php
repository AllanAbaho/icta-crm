<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Applications';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="applications-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Apply For Membership', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id:text:Application Number ',
            'category',
            'user.membership.name:text:Membership Type',
            'user.membership.fee:text:Membership Fee',
            'status',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]); ?>


</div>
