<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */
/* @var $form yii\widgets\ActiveForm */
$types = \app\models\MembershipTypes::find()->all();
?>

<div class="applications-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $form->errorSummary($user); ?>

    <?= $form->field($model, 'user_id')->hiddenInput(['value'=>$user->id])->label(false) ?>

    <?= $form->field($model, 'status')->hiddenInput(['value'=>'Review'])->label(false) ?>

    <?= $form->field($model, 'category')->hiddenInput(['value'=>'Membership'])->label(false) ?>

    <?= $form->field($user, 'membership_type_id')->dropDownList(ArrayHelper::map($types, 'id','name')) ?>

<div class="row">
        <div class="col-md-3">
            <?= $form->field($user, 'alternative_email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'occupation')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'organisation')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'organisation_size')->textInput(['type' => 'number']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($user, 'alternative_phone_number')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'other_phone_number')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'physical_address')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'city_of_residence')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($user, 'country_of_residence')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'facebook_handle')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'twitter_handle')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($user, 'city_of_residence')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($user, 'brief_bio')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($user, 'about_ictau')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($user, 'other_comments')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($user, 'reference')->textarea(['rows' => 6]) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
