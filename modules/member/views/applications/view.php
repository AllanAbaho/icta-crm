<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */

$this->title = $model->user->name;
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="applications-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <p> 
    <?php if($model->status == 'Review'): ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Submit Application', ['submit-application', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Are you sure you want to submit this application?',
            ],
        ]) ?>
    <?php endif; ?>
    <?php if($model->status == 'Pending Payment'): ?>
        <!--?= Html::a('Bank Payment', ['payments/create', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Are you sure you want to make payment for this application?',
            ],
        ]) ?-->
        <?= Html::a('Make Payment', ['payments/olycash', 'id' => $model->id], [
            'class' => 'btn btn-primary',
            'data' => [
                'confirm' => 'Are you sure you want to make payment for this application?',
            ],
        ]) ?>
    <?php endif; ?>

    </p>


    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id:text:Application Number ',
                    'category',
                    'user.membership.name:text:Membership Type',
                    'user.membership.fee:text:Membership Fee',
                    'status',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'user.email',
                    'user.primary_phone_number',
                    'user.occupation',
                    'user.physical_address',
                    'user.city_of_residence',
                ],
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'user.alternative_email',
                    'user.organisation',
                    'user.organisation_size',
                    'user.physical_address',
                    'user.alternative_phone_number',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'user.country_of_residence',
                    'user.facebook_handle',
                    'user.twitter_handle',
                    'user.physical_address',
                    'user.other_phone_number',
                ],
            ]) ?>
        </div>
    </div>


</div>
