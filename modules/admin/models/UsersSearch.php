<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

/**
 * UsersSearch represents the model behind the search form of `app\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'membership_type_id', 'created_by', 'updated_by'], 'integer'],
            [['first_name', 'last_name', 'email', 'alternative_email', 'occupation', 'organisation', 'organisation_size', 'primary_phone_number', 'alternative_phone_number', 'other_phone_number', 'physical_address', 'brief_bio', 'city_of_residence', 'country_of_residence', 'facebook_handle', 'twitter_handle', 'about_ictau', 'other_comments', 'reference', 'created_at', 'updated_at', 'role', 'status', 'password', 'auth_key', 'password_reset_token', 'is_notified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find()->where(['role'=>'Member']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'membership_type_id' => $this->membership_type_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'alternative_email', $this->alternative_email])
            ->andFilterWhere(['like', 'occupation', $this->occupation])
            ->andFilterWhere(['like', 'organisation', $this->organisation])
            ->andFilterWhere(['like', 'organisation_size', $this->organisation_size])
            ->andFilterWhere(['like', 'primary_phone_number', $this->primary_phone_number])
            ->andFilterWhere(['like', 'alternative_phone_number', $this->alternative_phone_number])
            ->andFilterWhere(['like', 'other_phone_number', $this->other_phone_number])
            ->andFilterWhere(['like', 'physical_address', $this->physical_address])
            ->andFilterWhere(['like', 'brief_bio', $this->brief_bio])
            ->andFilterWhere(['like', 'city_of_residence', $this->city_of_residence])
            ->andFilterWhere(['like', 'country_of_residence', $this->country_of_residence])
            ->andFilterWhere(['like', 'facebook_handle', $this->facebook_handle])
            ->andFilterWhere(['like', 'twitter_handle', $this->twitter_handle])
            ->andFilterWhere(['like', 'about_ictau', $this->about_ictau])
            ->andFilterWhere(['like', 'other_comments', $this->other_comments])
            ->andFilterWhere(['like', 'reference', $this->reference])
            ->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'is_notified', $this->is_notified]);

        return $dataProvider;
    }
}
