<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alternative_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'occupation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'organisation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'organisation_size')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'primary_phone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alternative_phone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'other_phone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'physical_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'brief_bio')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'membership_type_id')->textInput() ?>

    <?= $form->field($model, 'city_of_residence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'county_of_residence')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'facebook_handle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'twitter_handle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'about_ictau')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'other_comments')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'reference')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'role')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password_reset_token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_notified')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
