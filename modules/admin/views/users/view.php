<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'first_name',
            'last_name',
            'email:email',
            'alternative_email:email',
            'occupation',
            'organisation',
            'organisation_size',
            'primary_phone_number',
            'alternative_phone_number',
            'other_phone_number',
            'physical_address',
            'brief_bio:ntext',
            'membership_type_id',
            'city_of_residence',
            'county_of_residence',
            'facebook_handle',
            'twitter_handle',
            'about_ictau:ntext',
            'other_comments:ntext',
            'reference',
        ],
    ]) ?>

</div>
