<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */

$this->title = $model->user->name;
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="applications-view">
    <p> 
        <?php if($model->status == 'Pending Approval'): ?>
            <?= Html::a('Approve Application', ['approve-application', 'id' => $model->id], [
                'class' => 'btn btn-success',
                'data' => [
                    'confirm' => 'Are you sure you want to approve this application?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
        <?php if($model->status == 'Paid'): ?>
            <?= Html::a('Approve Payment', ['approve-payment', 'id' => $model->id], [
                'class' => 'btn btn-success',
                'data' => [
                    'confirm' => 'Are you sure you want to approve payment for this application?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>

    </p>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if($model->payment): ?>
        <div class="row">
            <div class="col-md-6">
                <img src="<?= Yii::getAlias('@web') .'/receipts/' . $model->payment->file ?>" class="img-responsive" />
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id:text:Application Number ',
                    'category',
                    'user.membership.name:text:Membership Type',
                    'user.membership.fee:text:Membership Fee',
                    'status',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'user.email',
                    'user.primary_phone_number',
                    'user.occupation',
                    'user.physical_address',
                    'user.city_of_residence',
                ],
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'user.alternative_email',
                    'user.organisation',
                    'user.organisation_size',
                    'user.physical_address',
                    'user.alternative_phone_number',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'user.country_of_residence',
                    'user.facebook_handle',
                    'user.twitter_handle',
                    'user.physical_address',
                    'user.other_phone_number',
                ],
            ]) ?>
        </div>
    </div>

</div>
