<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\PaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <!-- <p>
        <?= Html::a('Create Payments', ['create'], ['class' => 'btn btn-success']) ?>
    </p> -->

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'Member Name',
                'attribute' => 'user_id',
                'value' => function($data){
                    return $data->user->name;
                },
            ],
            [
                'label' => 'Category',
                'attribute' => 'application_id',
                'value' => function($data){
                    return $data->application->category;
                },
            ],
            'payment_type',
            'amount',
            //'status',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',
            //'name',
            //'phone_number',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
