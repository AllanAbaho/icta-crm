<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Licenses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="licenses-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'start_date')->textInput(['value'=>date('Y-m-d')]) ?>

    <?= $form->field($model, 'end_date')->textInput(['value'=>date('Y-12-31')]) ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
