ALTER TABLE `users` CHANGE `reference` `reference` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;
ALTER TABLE `applications` DROP `event_id`, DROP `membership_type_id`;
ALTER TABLE `applications` ADD `category` VARCHAR(100) NULL AFTER `user_id`;
ALTER TABLE `users` CHANGE `county_of_residence` `country_of_residence` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;
ALTER TABLE `licenses` ADD `file` VARCHAR(100) NULL AFTER `end_date`;
ALTER TABLE `payments` ADD `reference_number` VARCHAR(100) NULL AFTER `phone_number`, ADD `file` VARCHAR(100) NULL AFTER `reference_number`;
ALTER TABLE `events` ADD `file` VARCHAR(100) NULL AFTER `venue`;
