<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;


/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        return ExitCode::OK;
    }

    public function actionCreateAdmin(){
        $user = new \app\models\Users;
        $user->first_name = 'Ictau';
        $user->last_name = 'Admin';
        $user->email = 'admin@ictau.ug';
        $user->role = 'Admin';
        $user->primary_phone_number = '0700000000';
        $user->status = '10';
        $user->password = \Yii::$app->security->generatePasswordHash('Admin@123');
        $user->password_confirm = $user->password;

        if($user->save()){
            echo "Admin successfully created \n";
        }else{
            $errors =  $user->getFirstErrors();
            foreach($errors as $error){
                echo $error;
            }
        }
    }
}
