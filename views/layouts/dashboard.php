<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html> 
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    if( Yii::$app->user->isGuest){
        return Yii::$app->getResponse()->redirect(['/site/login']);
    }
    $role = Yii::$app->user->identity->role;
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            // ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Users', 'url' => ['/admin/users'], 'visible'=>$role=='Admin'],
            ['label' => 'Applications', 'url' => ['/member/applications'], 'visible'=>$role=='Member'],
            ['label' => 'Applications', 'url' => ['/admin/applications'], 'visible'=>$role=='Admin'],
            ['label' => 'Membership Types', 'url' => ['/admin/membership-types'], 'visible'=>$role=='Admin'],
            ['label' => 'Licenses', 'url' => ['/admin/licenses'], 'visible'=>$role=='Admin'],
            ['label' => 'Licenses', 'url' => ['/member/licenses'], 'visible'=>$role=='Member'],
            ['label' => 'Events', 'url' => ['/admin/events'], 'visible'=>$role=='Admin'],
            ['label' => 'Payments', 'url' => ['/admin/payments'], 'visible'=>$role=='Admin'],
            ['label' => 'Error Logs', 'url' => ['/super/logs'], 'visible'=>$role=='Super'],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->email . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
