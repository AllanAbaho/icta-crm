<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Create Account';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
<br><br>

<div class="site-login col-md-8 col-md-offset-2">
    <div class="card-header">
        <h3 class="card-title">Create Account</h3>
    </div>

    <p>Register here</p>
    <div class="card-body">

        <?php $form = ActiveForm::begin([
            // 'id' => 'login-form',
            // // 'layout' => 'horizontal',
            // 'fieldConfig' => [
            //     'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
            //     'labelOptions' => ['class' => 'col-lg-12'],
            // ],
        ]); ?>

        <div class="row">
            <div class="col-md-6">        
                <?= $form->field($model, 'first_name')->textInput(['placeholder'=>'First Name']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'last_name')->textInput(['placeholder'=>'Last Name']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput(['placeholder'=>'Email Address']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'primary_phone_number')->textInput(['placeholder'=>'Phone Number']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Password']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'password_confirm')->passwordInput(['placeholder'=>'Confirm Password']) ?>
            </div>
        </div>        

        <div class="form-group">
            <?= Html::submitButton('Create Account', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
        </div>

    </div>

    <?php ActiveForm::end(); ?>
    <br>

</div>
</div>
