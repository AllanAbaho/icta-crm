<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <!-- <!?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <!?= Yii::$app->session->getFlash('success') ?>
        </div>
    <!?php endif; ?> -->

<br><br>

<div class="site-login col-md-4 col-md-offset-4">
    <div class="card-header">
        <h3 class="card-title">Login</h3>
    </div>

    <p>Sign in to start your session</p>
    <div class="card-body">

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>

        <?= $form->field($model, 'email')->textInput() ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg-11\">{input} {label}</div>\n<div class=\"col-lg-12\">{error}</div>",
        ]) ?>

        <div class="form-group">
            <div class="col-lg-6">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
            </div>
            <div class="col-md-6">
                <?= Html::a('Forgotten Password?', ['/user/request-password-reset']);?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>
    <br>
    Have no account yet? <?= Html::a('Register Here', ['register']);?>
</div>
</div>
